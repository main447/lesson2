import java.util.Scanner;
public class Task2 {
    public static void main(String[] args) {
        int wdes;
        int tdes;
        String tadv;
        String wadv;
        String radv;
        Scanner myscan = new Scanner (System.in);
        System.out.println("Введите значение температуры");
        int temp = myscan.nextInt ();
        if (temp<-25){
            tdes=0;
            tadv = ", слишком холодно";
        }
        else if  (temp<10){
            tdes=1;
            tadv = ", но стоит одеться теплее";
        }
        else if  (temp<35){
            tdes=1;
            tadv = "";
        }
        else{
            tdes=0;
            tadv = ", слишком жарко";
        }
        System.out.println("Введите скорость ветра");
        int wind = myscan.nextInt ();
        if (wind<14){
            wdes=1;
            wadv = "";
        }
        else {
            wdes=0;
            wadv = ", сильный ветер";
        }
        System.out.println("Идет ли дождь? (1-'да'/2-'нет')");
        int rain = myscan.nextInt ();
        if (rain==1){
            radv = " и возьмите зонт";
        }
        else {
            radv = "";
        }
        if ((wdes==1)&&(tdes==1)){
            System.out.print("Да");
            System.out.print(tadv);
            System.out.print(wadv);
            System.out.print(radv);
        }
        else{
            System.out.print("Нет");
            System.out.print(tadv);
            System.out.print(wadv);
        }
    }
}
